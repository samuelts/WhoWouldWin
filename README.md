# ReactDrumMachine

A virtual drumkit written in react

Demo here: [Drum Machine](https://samuelts.com/ReactDrumMachine)

### Images

![Drum Machine](https://raw.githubusercontent.com/safwyls/logos/master/DrumMachine.png)
