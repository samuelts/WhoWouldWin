import React, { Component } from 'react';

const API_KEY = '79faca333e5f3d337de16faaf620b854';
const TOTAL_CHAR = 1491;

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      char1: {
        name: '',
        img: '',
        desc: ''
      },
      char2: {
        name: '',
        img: '',
        desc: ''
      },
      error: false,
      errorMsg: ''
    };

    this.fetchChar = this.fetchChar.bind(this);
    this.getRandomInt = this.getRandomInt.bind(this);
    this.getContenders = this.getContenders.bind(this);
    this.win = this.win.bind(this);
  }

  componentWillMount() {
    this.getContenders();
  }

  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  fetchChar() {
    return new Promise((resolve, reject) => {
      let result;
      let request = new XMLHttpRequest();
      let API_URL = `https://gateway.marvel.com:443/v1/public/characters?limit=1&offset=${this.getRandomInt(TOTAL_CHAR)}&apikey=${API_KEY}`;
      request.onerror = reject;
      request.open('GET', API_URL, true);
      request.onreadystatechange = () => {
        if (request.readyState === 4) {
          let json = JSON.parse(request.response);
          if (request.status === 200) {
            result = json.data.results[0];
            let thumbnail = result.thumbnail;
            resolve({
              name: result.name,
              img: `${thumbnail.path}/portrait_uncanny.${thumbnail.extension}`,
              desc: result.description
            });
          } else {
            this.setState({
              error: true,
              errorMsg: `${json.code}: ${json.status}`
            });
          }
        }
      }
      request.send();
    })
  }

  getContenders() {
    this.fetchChar()
      .then((char) => {
        this.setState({
          char1: char
        });
      });
    this.fetchChar()
      .then((char) => {
        this.setState({
          char2: char
        });
      })
  }

  win(e) {
    alert("You chose "+e.target.nextSibling.innerText);
  }

  render() {
    return (
      <div className='container'>
        <div id="header">Who Would Win? - Marvel Edition</div>
        <div id='arena'>
          <div className='char' id='char1' onClick={this.win}>
            <img src={this.state.char1.img} />
            <div className='name'>{this.state.char1.name}</div>
          </div>
          <div id='vs-text'>VS</div>
          <div className='char' id='char2' onClick={this.win}>
            <img src={this.state.char2.img} />
            <div className='name'>{this.state.char2.name}</div>
          </div>
        </div>
        <button id='init-btn'onClick={this.getContenders}>Showdown!</button>
        <div id='error-status'>{this.state.error ? this.state.errorMsg : null}</div>
      </div>
    );
  }
}
